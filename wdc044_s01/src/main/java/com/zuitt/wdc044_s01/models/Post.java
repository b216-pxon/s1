package com.zuitt.wdc044_s01.models;


import javax.persistence.*;

// mark this java object as a representation of a database via @Entity.
@Entity
// designate table name
@Table(name = "posts")
public class Post {
    // indicate that this property represents the primary key
    @Id
    // values for this property will be auto-incremented
    @GeneratedValue
    private Long id;
    // class properties that represent table columns in a relational database are annotated as @Column
    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    // Empty constructor
    public Post(){}

    // Parameterized constructor
    public Post (String title, String content){
        this.title = title;
        this.content = content;
    }

    // getters and setters

    public String getTitle(){
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
